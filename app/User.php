<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Cara Laravel 
    // One to One
    public function address()
    {
        return $this->hasOne(Address::class);
        // return $this->hasOne(Address::class, 'user'); // bisa juga kyk gini jika field user_id diganti dengan user
    }

    // Cara PHP
    // public function countryName()
    // {
    //     return Address::where('user_id', $this->id)->first()->country;
    // }
    
    // One to Many
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    
}
