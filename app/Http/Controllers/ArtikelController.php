<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class ArtikelController extends Controller
{
    public function index() 
    {
        $posts = Post::get();
        return view('posts.index', compact('posts'));
    }
}
